<?php

DEFINE("PUBLIC_DIR", __DIR__);
DEFINE( 'ROOT', dirname(dirname(__FILE__)) . '/' );

require __DIR__ . '/../vendor/autoload.php';

error_reporting(E_ALL);

use \MP\App;

App::initialize();

?>
