<?php

namespace MP\Controllers\V1;

use \MP\Request;
use \MP\Response;
use \MP\Models\Product;

class Products {

  static function index() {
      $products = Product::getAll();
      Response::send(["products" => $products], 200);
  }
  static function create() {
      try {
          $product = Product::createNewProduct();
          Response::send(["product" => $product], 200);
      } catch (Exception $e) {
          return Response::send(["errors" => "Something went wrong when creating product."], 500);
      }
  }
  static function update() {
      $product = [
        "id" => Request::get("id", ["integer"], true),
        "name" => Request::get("name", ["not_empty"], true),
        "weight" => Request::get("weight", ["numeric"], true),
        "height" => Request::get("height", ["numeric"], true),
        "width" => Request::get("width", ["numeric"], true),
        "length" => Request::get("length", ["numeric"], true),
      ];
      if (Request::hasErrors()) {
          return Response::send(["errors" => Request::getErrors()], 422);
      }
      try {
          Product::updateProduct($product);
          return Response::send(["product" => $product], 200);
      } catch (Exception $e) {
          return Response::send(["errors" => "Something went wrong when updating product."], 500);
      }
  }
  static function delete() {
      $id = Request::get("id", ["integer"], true);

      if (Request::hasErrors()) {
        return Response::send(["errors" => Request::getErrors()], 422);
      }
      try {
          Product::deleteProduct($id);
          return Response::send(["message" => "success"], 200);
      } catch (Exception $e) {
          return Response::send(["errors" => "Something went wrong when deleting product."], 500);
      }
  }
}
