<?php

namespace MP;

use PDO;

class DB {
    private static $c;
    private static $dbName;

    public static function startConnection($dbName, $userName, $password) {
        if (self::$c !== null) {
            return;
        }
        self::$dbName = $dbName;
        self::$c = new PDO("mysql:host=localhost;dbname=$dbName", $userName, $password);
        self::$c->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    }

    public static function makeWhereInList($arr) {
      return $arr = '(' . implode(',', $arr) . ')';
    }

    public static function prepareDatabase() {
        $version = self::setUpVersionTable();
        $originalVersion = $version;

        if ($version < 1.01) {
            self::createProductsTable();
            $version = 1.01;
        }
        if ($version != $originalVersion) {
            self::addVersion($version);
        }
    }

    public static function getConnection() {
        return self::$c;
    }

    private static function addVersion($number) {
        $c = self::$c;
        $c->exec("insert into versions (number) values($number)");
    }

    private static function setUpVersionTable() {
        $c = self::$c;
        $dbName = self::$dbName;
        $rows = $c->query("show tables where Tables_in_$dbName = 'versions'");
        if ($rows->rowCount() == 0) {
            $c->exec(
                "create table versions (
                number double not null,
                unique key (number))");
            $c->exec("insert into versions (number) values (1.0)");
        }
        return floatval($c->query("select max(number) as number from versions")->fetch()["number"]);
    }

    private static function createProductsTable() {
        $c = self::$c;
        $c->exec(
            "create table products (
            id int(11) auto_increment primary key,
            name varchar(30) not null default 'untitled',
            weight double not null default 0,
            width double not null default 0,
            length double not null default 0,
            height double not null default 0)");
    }
}


?>
