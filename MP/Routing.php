<?php

namespace MP;
use \MP\Request;
use \MP\Response;

require __DIR__ . '/routes/routes.php';

class Routing {
    static $routes = [
      "GET" => [],
      "POST" => [],
    ];

    static function isApiRequest() {
        $path = $_SERVER['REQUEST_URI'];
        return preg_match('/^\/api.*/', $path);
    }
    static function processRoute() {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "GET") {
            self::routeToController("GET");
        } else if ($method == "POST") {
            self::routeToController("POST");
        }
    }
    /*
    * Type: GET or POST
    * Path: The path to match when we receive a request
    * Handler: A string representing a class and mehtod that we should call when the path matches
    */
    static function route($type, $path, $handler) {
        self::$routes[strtoupper($type)][$path] = $handler;
    }
    static function routeToController($method) {
        list($path) = explode("?", $_SERVER['REQUEST_URI']);
        foreach (self::$routes[$method] as $route => $handler) {
            if ($path == $route) {
                $handler();
                return;
            }
        }
        // if we reached here, none of the routes match the path
        Response::send([], 404);
    }
}
