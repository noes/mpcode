<?php

namespace MP;

class Env {
    private static $vars = [];
    public static function getVars() {
        $envFile = ROOT . '.env';
        $handle = fopen($envFile, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $pos = strpos($line, '=');
                $key = substr($line, 0, $pos);
                $val = trim(substr($line, $pos + 1));
                self::$vars[$key] = $val;
            }
            fclose($handle);
        } else {
            // error opening the file.
        }
    }
    public static function get($key) {
        return self::$vars[$key];
    }
}

Env::getVars();

?>
