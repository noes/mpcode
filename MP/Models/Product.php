<?php

namespace MP\Models;

use MP\DB;
use PDO;

class Product {
  static function getAll() {
      $db = DB::getConnection();
      $stmt = $db->prepare("select * from products");
      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }
  static function createNewProduct() {
      $db = DB::getConnection();
      $stmt = $db->prepare("insert into products values ()");
      $stmt->execute();
      $lastId = $db->lastInsertId();
      $stmt = $db->prepare("select * from products where id = ?");
      $stmt->execute([$lastId]);
      return $stmt->fetch(PDO::FETCH_ASSOC);
  }
  static function updateProduct($data) {
      $db = DB::getConnection();
      $stmt = $db->prepare("update products set
                            name = ?,
                            weight = ?,
                            height = ?,
                            width = ?,
                            length = ?
                            where id = ?");
      $stmt->execute([$data["name"], $data["weight"], $data["height"], $data["width"], $data["length"], $data["id"]]);
  }
  static function deleteProduct($id) {
      $db = DB::getConnection();
      $stmt = $db->prepare("delete from products where id = ?");
      $stmt->execute([$id]);
  }
}
