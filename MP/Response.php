<?php

namespace MP;

class Response {

  static function send($arr = [], $code = 200) {
      http_response_code($code);
      header("Content-type: application/json");
      echo json_encode($arr);
      exit();
  }
}
