<?php

namespace MP;

use \MP\Routing;
use \MP\Env;
use \MP\DB;

class App {
    /*
    *  This sets up the database and begins the routing of the request
    */
    static function initialize() {

      // we send the html file if this isn't an api call
      if (!Routing::isAPIRequest()) {
        header("Content-type: text/html");
        readfile(PUBLIC_DIR . '/index.html');
        exit(0);
      }
      DB::startConnection(Env::get("DB_DATABASE"), ENV::get("DB_USERNAME"), ENV::get("DB_PASSWORD"));
      DB::prepareDatabase();
      Request::setParams();
      Routing::processRoute();
    }
}
