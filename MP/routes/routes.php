<?php

use \MP\Routing;
use \MP\Controllers\V1;

Routing::route("GET", "/api/products/index", V1\Products::class . "::index");
Routing::route("POST", "/api/products/create", V1\Products::class . "::create");
Routing::route("POST", "/api/products/update", V1\Products::class . "::update");
Routing::route("POST", "/api/products/delete", V1\Products::class . "::delete");
