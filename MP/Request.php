<?php

namespace MP;
use MP\Response;

class Request {
    private static $params;

    private static $validationErrors = [];

    static function setParams() {
        if ($_SERVER['REQUEST_METHOD'] == "GET") {
            self::$params = $_GET;
        } else if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $params = json_decode(file_get_contents('php://input'), true);
            self::$params = $params;
        }
    }
    static function get($paramName, $rules, $required) {
        if (!isset(self::$params[$paramName])) {
            if ($required) {
                self::$validationErrors[] = "Missing $paramName";
            } else {
                return null;
            }
        }
        $param = self::$params[$paramName];
        foreach ($rules as $rule) {
            switch ($rule) {
                case "integer": {
                    if (!is_numeric($param) || intval($param) != doubleval($param)) {
                        self::$validationErrors[$paramName] = "$paramName isn't an integer.";
                    }
                    break;
                }
                case "numeric": {
                    if (!is_numeric($param)) {
                        self::$validationErrors[$paramName] = "$paramName isn't numeric.";
                    }
                    break;
                }
                case "not_empty": {
                    if ($param == "") {
                        self::$validationErrors[$paramName] = "$paramName is empty.";
                    }
                    break;
                }
            }
        }
        return $param;
    }
    static function hasErrors() {
      return count(self::$validationErrors) > 0;
    }
    static function getErrors() {
      return self::$validationErrors;
    }
}
