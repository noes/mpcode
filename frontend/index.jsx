import 'babel-polyfill';

require("./scss/app.scss");

import React from 'react';
import ReactDom from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './js/App.jsx';


let root = document.getElementById("react-root");

ReactDom.render(
  <Router>
    <App />
  </Router>,
  root
);
