import React from 'react';
import Modal from './Modal.jsx';

class SimpleAlert extends React.Component {
  constructor() {
    super();
  }
  render() {
    console.log(this.props);
    return (
      <Modal onClick={this.props.onClose}>
          <div className="modalContent">
            <h2>{this.props.mainText}</h2>
            {this.props.subText && <h3>{this.props.subText}</h3>}
            <div className="modalButtons">
              <button onClick={this.props.onClose}>OK</button>
            </div>
          </div>
      </Modal>
    )
  }
}

export default SimpleAlert;
