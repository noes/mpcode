import React from 'react';
import api from './api.js';
import SimpleAlert from './SimpleAlert.jsx';

class App extends React.Component {
  constructor() {
    super();

    this.state = {
      products: [],
      selectedProduct: null,
      formData: null,
      errors: {},
      alertMessage: null,
    };
  }
  componentDidMount() {
    api.getProducts()
    .then((results) => {
      this.setState({ products: results.products });
    });
  }
  selectProduct(id) {
    let item = this.state.products.find((prod) => prod.id == id);
    this.setState({
      selectedProduct: id,
      formData: {
        ...item
      }
    });
  }
  onInput(e) {
    let value = e.target.value;
    let name = e.target.name;
    let formData = { ...this.state.formData };
    let errors = { ...this.state.errors };
    formData[name] = value;
    delete errors[name];
    this.setState({ formData, errors });
  }
  validateForm() {
    let inputs = this.state.formData;
    let errors = {};
    if (inputs.name == "") {
      errors.name = "Name can't be empty.";
    }
    if (inputs.name.length > 30) {
      errors.name = "Name can't be over 30 characters.";
    }
    if (isNaN(+inputs.weight)) {
      errors.weight = "Weight must be a number";
    }
    if (isNaN(+inputs.height)) {
      errors.height = "Height must be a number";
    }
    if (isNaN(+inputs.width)) {
      errors.width = "Width must be a number";
    }
    if (isNaN(+inputs.length)) {
      errors.length = "length must be a number";
    }
    this.setState({errors});
    return Object.keys(errors).length == 0;
  }
  updateProduct(e) {
    e.preventDefault();
    if (!this.validateForm()) {
      return;
    }
    api.updateProduct(this.state.formData)
    .then(({ product }) => {
      let products = [...this.state.products];
      products.splice(products.findIndex((a) => a.id == product.id), 1, product);
      this.setState({
        products,
        alertMessage: { mainText: "Success", subText: "Product updated."}
      });
    })
    .catch(({response, status}) => {
      console.log(status);
      if (status == 422) {
        this.setState({ errors: response.errors });
      } else {
        this.setState({
          alertMessage: { mainText: "Sorry", subText: "Something went wrong."}
        });
      }
    });
  }
  createProduct() {
    api.createProduct()
    .then(({product}) => {
      this.setState({
        products: [...this.state.products, product],
        alertMessage: { mainText: "Success", subText: "Product created."}
      });
    })
    .catch(({response, status}) => {
      if (status == 422) {
        this.setState({ errors: response.errors });
      } else {
        this.setState({
          alertMessage: { mainText: "Sorry", subText: "Something went wrong."}
        });
      }
    })
  }
  deleteProduct() {
    let productId = this.state.selectedProduct;
    api.deleteProduct(productId)
    .then(() => {
      let products = [...this.state.products];
      products.splice(products.findIndex((prod) => prod.id == productId), 1);
      this.setState({
        products, selectedProduct: null,
        alertMessage: { mainText: "Success", subText: "Product deleted."}
      });
    })
    .catch(({response, status}) => {
      if (status == 422) {
        this.setState({ errors: response.errors });
      } else {
        this.setState({
          alertMessage: { mainText: "Sorry", subText: "Something went wrong."}
        });
      }
    });
  }
  render() {
    let products = this.state.products.map((product, index) => {
      let onClick = () => this.selectProduct(product.id);
      let className = product.id == this.state.selectedProduct ? 'productListItem selected' : 'productListItem';
      return <div key={index} onClick={onClick} className={className}>{product.name}</div>
    })
    let selectedProduct = this.state.products.find((prod) => prod.id == this.state.selectedProduct);
    return (
      <React.Fragment>
        <header>
          <h1>MP Product Setting Tool</h1>
        </header>
        <div className="productColumns">
          <div className="productList">
            <h2>Product List</h2>
            <div className="productListItems">
              { products }
              { products.length == 0 && <p>Empty</p> }
            </div>
          </div>
          <div className="editingBox">
            <h2>Edit Products</h2>
            <button onClick={() => this.createProduct()}>Create new Product</button>
            {
              selectedProduct &&
              <div className="selectedProduct">
                <h3>Selected Product</h3>
                <form onSubmit={this.updateProduct.bind(this)} className="editingBoxForm">
                  {
                    ["name", "weight", "height", "width", "length"].map((name) => {
                      return (
                        <label>
                          {name.toUpperCase()}: <input onInput={this.onInput.bind(this)} name={name} type="text" value={this.state.formData[name]} />
                          { this.state.errors[name] && <p className="errorLabel">{this.state.errors[name]}</p> }
                        </label>
                      )
                    })
                  }
                  <button>SAVE</button>
                </form>
                <button onClick={() => this.deleteProduct()}>DELETE</button>
              </div>
            }
          </div>
        </div>
        {
          this.state.alertMessage &&
          <SimpleAlert onClose={() => this.setState({alertMessage: null})} {...this.state.alertMessage} />
        }
      </React.Fragment>
    )
  }
}

export default App;
