
let api = {
  getReq: function(url, data = {}, signal = null) {
    const params = new URLSearchParams();
    for (let key in data) {
      params.append(key, data[key]);
    }
    url = url + `?${params.toString()}`;
    return fetch(url)
    .then((results) => {
      return results.json()
      .then((json) => {
        if (results.ok) {
          return json;
        } else {
          return Promise.reject({response: json, status: results.status});
        }
      });
    });
  },
  postReq: function(url, data = {}) {
    let body = JSON.stringify(data);
    return fetch(
    url,
    {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body
    })
    .then((results) => {
      return results.json()
      .then((json) => {
        if (results.ok) {
          return json;
        } else {
          return Promise.reject({response: json, status: results.status});
        }
      });
    });
  },
  getProducts() {
    return this.getReq("api/products/index");
  },
  createProduct() {
    return this.postReq("/api/products/create");
  },
  updateProduct(product) {
    return this.postReq("/api/products/update", product);
  },
  deleteProduct(productId) {
    return this.postReq("/api/products/delete", { id: productId });
  }
}

export default api;
