const path = require("path");
const webpack = require("webpack");

module.exports = {
  mode: 'production',
  devtool: 'source-map',
  entry: {
    "/js/app.js": "./frontend/index.jsx",
  },
  output: {
    path: path.join(__dirname, 'public'),
    filename: '[name]',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: [
          { loader: 'babel-loader' }
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'app.css',
              outputPath: 'css'
            }
          },
          { loader: 'extract-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' },
        ]
      }
    ]
  },
  watchOptions: {
    poll: 1000,
  }
};
